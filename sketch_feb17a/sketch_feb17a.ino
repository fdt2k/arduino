
#include <Wire.h>

char input;

String command = "";

byte slave;

enum state {
   idle,
   scan,
   password  
 };

char convTable[256] = {
    ' ', ' ', '1', '2', '3', '4', '5', '6',    '7', '8', '9', '0', '-', '=', ' ', ' ', 
    'Q', 'W', 'E', 'R', 'T', 'Y', 'U', 'I',    'O', 'P', ' ', '$', ' ', ' ', 'A', 'S', 
    'D', 'F', 'G', 'H', 'J', 'K', 'L', ';',    '\'','`', '\\','\\','Z', 'X', 'C', 'V', 
    'B', 'N', 'M', ',', '.', '/', '"', '*',    ' ', ' ', ' ', ' ', 'f', 'f', 'f', 'f',
    
    'f', 'f', 'f', 'f', 'f', ' ', ' ', '7',    '8', '9', '-', '4', '5', '6', '+', '1',
    '2', '3', '0', '.', ' ', ' ', '>', ' ',    ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ',
    ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ',    ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ',
    ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ',    ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ',
    
    ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ',    ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ',
    ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ',    ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ',
    ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ',    ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ',
    ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ',    ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ',
    
    ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ',    ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ',
    ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ',    ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ',
    ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ',    ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ',
    ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ',    ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ',
};

state current_state = idle;

void setup() {
  // put your setup code here, to run once:
  Serial.begin(115200); 
    delay(2000);  
 
    Serial.println("Type something!");
}

void loop() {
  // put your main code here, to run repeatedly:
  if(current_state == idle && Serial.available()){
      
      input = Serial.read();
      if (input != '\n'){
        command.concat(input);
      }else{
        Serial.println(command);
        if(command == "scan"){
          current_state=scan;  
        }else if(command =="password"){
          current_state=password; 
        }else{
          current_state=idle;
          Serial.println("Unkown command");
    
        }
        command = String("");
      }
     // Serial.print("You typed: " );
     // Serial.println(input);
     
  }else if(current_state == scan){
    Serial.println("scanning port");   
    cmd_scan();
  }else if (current_state == password){
    cmd_password();
    
   }
}


void cmd_scan (){
  
 Wire.begin();
 
 byte count = 0;
 for (byte i = 1; i < 255; i++)
  {
    Wire.beginTransmission (i);
    if (Wire.endTransmission () == 0)
    {
      Serial.print ("Found address: ");
      Serial.print (i, DEC);
      Serial.print (" (0x");
      Serial.print (i, HEX);
      Serial.println (")");
      slave =i ;
      break;
    } // end of good response
    count++;
  } // end of for loop
  if(slave == NULL){
    Serial.println("No slave found"); 
  }
  current_state=idle;
  delay(500);
  }


void cmd_password(){
  byte count = 0;
  if (slave != NULL){
    Wire.requestFrom(slave, 0x38+8); 
    while (Wire.available()) {
      char c = Wire.read();    // receive a byte as character
      Serial.print("[RECV]");
      Serial.print(count,DEC);
      Serial.print(":");
      Serial.print(c,DEC);         // print the character
      Serial.print(" - 0x");         // print the character
      Serial.println(c,HEX);         // print the character
      count++;
    }
  }else{
    Serial.println("Scan First");
    
   }
  
   current_state=idle;
  delay(500);
  
}
